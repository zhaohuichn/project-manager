package com.project.manager.parse.excel.sheet.cell;

import com.project.manager.parse.excel.sheet.StandardExcelSheetParser;
import com.project.manager.source.descriptor.StandardExcelCellDescriptor;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.cell.StandardExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.util.ExcelUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * 单元格列解析
 *
 * @author ZHAOHUI
 */
public class StandardExcelCellParser implements ExcelCellParser<Cell, ExcelCell> {

    private ExcelRow excelRow;

    public StandardExcelCellParser(ExcelRow excelRow) {
        this.excelRow = excelRow;
    }

    @Override
    public ExcelCell parse(Cell raw) {
        StandardExcelCell excelCell = new StandardExcelCell();

        excelCell.setExcelRow(excelRow);

        int index = raw.getColumnIndex();
        excelCell.setColumnNo(index);

        int rowNo = raw.getRowIndex();
        String value = ExcelUtil.getCellStringValue(raw);
        int preRowNo = rowNo - 1;
        while (StringUtils.isBlank(value) && preRowNo > 0) {
            Row row = raw.getSheet().getRow(preRowNo--);
            value = ExcelUtil.getCellStringValue(row.getCell(index));
            if (StringUtils.isNotBlank(value)) {
                break;
            }
        }
        excelCell.setValue(value);

        excelCell.setDescriptor(new StandardExcelCellDescriptor(excelCell, raw));

        return excelCell;
    }
}
