package com.project.manager.parse.excel.sheet.row;

import com.project.manager.parse.excel.sheet.cell.ExcelCellParser;
import com.project.manager.parse.excel.sheet.cell.StandardExcelCellParser;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.source.excel.row.StandardRow;
import com.project.manager.source.excel.sheet.ExcelSheet;
import org.apache.commons.compress.utils.Lists;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.Iterator;
import java.util.List;

/**
 * @author ZHAOHUI
 */
public class StandardExcelRowParser implements ExcelRowParser {

    private ExcelSheet excelSheet;

    public StandardExcelRowParser(ExcelSheet sheet) {
        this.excelSheet = sheet;
    }

    @Override
    public ExcelRow parse(Row raw) {
        StandardRow row = new StandardRow();
        // 所属sheet页
        row.setSheet(this.excelSheet);
        // 行号
        int rowNum = raw.getRowNum();
        row.setRowNo(rowNum);
        row.setHeader(0 == rowNum);
        // 列值
        List<ExcelCell> cells = Lists.newArrayList();
        Iterator<Cell> cellIterator = raw.cellIterator();
        while (cellIterator.hasNext()) {
            cells.add(new StandardExcelCellParser(row).parse((cellIterator.next())));
        }
        row.setCells(cells);

        return row;
    }
}
