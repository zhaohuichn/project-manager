package com.project.manager.parse.excel.sheet.row;

import com.project.manager.parse.Parser;
import com.project.manager.source.excel.row.ExcelRow;
import org.apache.poi.ss.usermodel.Row;

/**
 * ExcelRowParser
 *
 * @author ZHAOHUI
 */
public interface ExcelRowParser extends Parser<Row, ExcelRow> {


}
