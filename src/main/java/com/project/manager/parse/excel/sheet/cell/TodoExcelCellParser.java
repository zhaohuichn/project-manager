package com.project.manager.parse.excel.sheet.cell;

import com.project.manager.source.excel.cell.ExcelCell;

/**
 * AbstractTodoExellCellParser
 *
 * @author ZHAOHUI
 */
public interface TodoExcelCellParser extends ExcelCellParser<ExcelCell, ExcelCell> {

}
