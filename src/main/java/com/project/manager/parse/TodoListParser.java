package com.project.manager.parse;

/**
 * 待办事项解析接口
 *
 * @author zhaohui
 */
public interface TodoListParser<Raw, Rs> extends Parser<Raw, Rs> {
}
