package com.project.manager.parse;

import com.google.common.collect.Lists;
import com.project.manager.configuration.PartConfiguration;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.source.excel.sheet.ExcelSheet;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 部门解析器
 * <p>
 * 将已配置的关键字与部门做映射
 * <p>
 * 每个关键字可能对应多个人员安排部门
 *
 * @author zhaohui
 */
public class PartParser implements Parser<ExcelSheet, PartConfiguration> {

    @Override
    public PartConfiguration parse(ExcelSheet sheet) {
        PartConfiguration confiuration = new PartConfiguration();

        List<ExcelRow> rows = sheet.getRows();
        for (ExcelRow row : rows) {
            ExcelCell schedulePartKeyWord = row.getCellByName("schedulePartKeyWords");
            String cusKey = schedulePartKeyWord.getValue();
            ExcelCell memberPartKeyWord = row.getCellByName("memberPartKeyWord");
            String[] cusParts = memberPartKeyWord.getValue().split("\\|");
            List<String> parts = confiuration.getOrDefault(cusKey, Lists.newArrayList());
            parts.addAll(Arrays.asList(cusParts));

            confiuration.put(cusKey, parts);
        }

        for (Map.Entry<String, List<String>> entry : confiuration.entrySet()) {
            List<String> value = entry.getValue().stream().distinct().filter(StringUtils::isNotBlank).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(value)) {
                confiuration.put(entry.getKey(), value);
            }
        }
        return confiuration;
    }


}
