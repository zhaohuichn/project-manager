package com.project.manager.builder;

import com.project.manager.configuration.Configuration;
import com.project.manager.parse.excel.StandardExcelParser;
import com.project.manager.source.excel.Excel;

import java.io.File;


/**
 * ConfigurationBuilder
 *
 * @author zhaohui
 */
public class ConfigurationBuilder implements Builder<Configuration> {

    private String path;

    public ConfigurationBuilder(String path) {
        this.path = path;
    }

    @Override
    public Configuration build() {
        Excel configExcel = new StandardExcelParser().parse(new File(path));
        return null;
    }
}
