package com.project.manager.builder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.project.manager.enums.TodoExcelColumnEnum;
import com.project.manager.factory.ExcelCellBuilderFactory;
import com.project.manager.source.descriptor.StandardExcelCellDescriptor;
import com.project.manager.source.excel.Excel;
import com.project.manager.source.excel.StandardExcel;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.cell.StandardExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.source.excel.row.StandardRow;
import com.project.manager.source.excel.sheet.ExcelSheet;
import com.project.manager.source.excel.sheet.StandardSheet;
import com.project.manager.util.ExcelUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * TodoExcelBuilder
 *
 * @author zhaohui
 */
public class TodoExcelBuilder implements Builder<Excel> {

    private static final Map<Integer, String> COLUMN_INDEX_NAME_MAP = Maps.newHashMap();

    static {
        COLUMN_INDEX_NAME_MAP.put(0, "经办人");
        COLUMN_INDEX_NAME_MAP.put(1, "所属模块");
        COLUMN_INDEX_NAME_MAP.put(2, "需求名称");
        COLUMN_INDEX_NAME_MAP.put(3, "Jira链接");
        COLUMN_INDEX_NAME_MAP.put(4, "待办类型");
        COLUMN_INDEX_NAME_MAP.put(5, "待办事项");
    }

    private Excel srcExcel;

    public TodoExcelBuilder(Excel src) {
        this.srcExcel = src;
    }

    @Override
    public Excel build() {
        Excel excel = obtainExcel();
        buildSheet(excel);
        return excel;
    }

    private void buildSheet(Excel excel) {
        StandardSheet sheet = new StandardSheet();
        sheet.setIndex(0);
        sheet.setName("今日待办");
        sheet.setExcel(excel);
        Map<Integer, String> columnMap = Arrays.stream(TodoExcelColumnEnum.values())
                .collect(Collectors.toMap(TodoExcelColumnEnum::getIndex, TodoExcelColumnEnum::getName));
        sheet.setColumnIndexNameMap(columnMap);
        setSheetRows(sheet);
        excel.addSheet(sheet);
    }

    /**
     * 设置行数据
     *
     * @param sheet 数据页
     */
    private void setSheetRows(StandardSheet sheet) {
        // build header
        ExcelRow headerRow = ExcelUtil.buildSheetHeader(TodoExcelColumnEnum.getMap());
        if (null == headerRow) {
            return;
        }
        sheet.setHeader(headerRow);
        // build data row
        List<ExcelRow> dataRows = buildSheetDataRows(sheet);
        sheet.setDataRows(dataRows);
        // set rows
        sheet.getRows().sort(Comparator.comparing(ExcelRow::getRowNo));
    }

    /**
     * 构造数据行
     * <p>
     * 行号从1开始
     *
     * @param sheet
     * @return
     */
    private List<ExcelRow> buildSheetDataRows(StandardSheet sheet) {
        // add data row
        ExcelSheet dataSheet = srcExcel.getSheet(0);
        // build data rows
        List<ExcelRow> excelRows = Lists.newArrayList();
        for (ExcelRow dataRow : dataSheet.getDataRows()) {
            // skip header
            if (0 == dataRow.getRowNo()) {
                continue;
            }
            System.out.println("构建数据。rowNo = " + dataRow.getRowNo());
            int rowNum = CollectionUtils.size(sheet.getRows());
            // 数据行可能需要拆分出多个行
            StandardRow targetRow = new StandardRow();
            targetRow.setRowNo(rowNum);
            targetRow.setSheet(sheet);
            targetRow.setHeader(false);
            List<ExcelCell> excelCells = buildCells(targetRow, dataRow);
            targetRow.setCells(excelCells);
            boolean isRetain = excelCells.stream().allMatch(c -> StringUtils.isNotBlank(c.getValue()));
            if (isRetain) {
                excelRows.add(targetRow);
            }
        }
        List<ExcelRow> rowAfterSplit = splitRows(excelRows);
        resetRowNo(rowAfterSplit);
        return rowAfterSplit;
    }

    private void resetRowNo(List<ExcelRow> excelRows) {
        // 先以人名排序
        List<ExcelRow> sortedRows = excelRows.stream().filter(r -> !r.isHeader())
                .sorted(Comparator.comparing(o -> o.getCellByName(TodoExcelColumnEnum.MEMBER.getName()).getValue()))
                .collect(Collectors.toList());
        // 分配序号
        int index = 1;
        for (ExcelRow sortedRow : sortedRows) {
            if (sortedRow.isHeader()) {
                sortedRow.setRowNo(0);
            } else {
                sortedRow.setRowNo(index++);
            }
        }
    }


    private List<ExcelRow> splitRows(List<ExcelRow> rows) {
        List<ExcelRow> rs = Lists.newArrayList();
        rs.addAll(splitByMember(rows));
        return rs;
    }

    private List<ExcelRow> splitByMember(List<ExcelRow> rows) {
        List<ExcelRow> targetRows = Lists.newArrayList();
        for (ExcelRow row : rows) {
            ExcelCell memCell = row.getCellByName(TodoExcelColumnEnum.MEMBER.getName());
            String members = memCell.getValue();
            boolean needSplit = members.contains(";");
            if (needSplit) {
                // 一行变成多行，每个人名变成一行
                // 除了人员列，其他列值都相同
                String[] memArray = members.split(";");
                for (String m : memArray) {
                    StandardRow newRow = new StandardRow();
                    newRow.setHeader(row.isHeader());
                    newRow.setRowNo(row.getRowNo());
                    newRow.setSheet(row.getSheet());
                    List<ExcelCell> cells = Lists.newArrayList();
                    row.getCells().forEach(c -> {
                        StandardExcelCell nc = new StandardExcelCell();
                        nc.setExcelRow(row);
                        nc.setColumnNo(c.getColumnNo());
                        nc.setDescriptor(new StandardExcelCellDescriptor());
                        if (c.getColumnNo() != TodoExcelColumnEnum.MEMBER.getIndex()) {
                            nc.setValue(c.getValue());
                        } else {
                            nc.setValue(m);
                        }
                        cells.add(nc);
                    });

                    newRow.setCells(cells);
                    targetRows.add(newRow);
                }
            } else {
                targetRows.add(row);
            }
        }
        return targetRows;
    }

    /**
     * 构建单元格
     * <p>
     * 每个单元格数据的生成方式都有差异，为了保持处理方式的统一，此处全部由builder处理
     *
     * @param toRow toRow
     * @return cell
     */
    private List<ExcelCell> buildCells(ExcelRow toRow, ExcelRow srcRow) {
        List<ExcelCell> cells = Lists.newArrayList();

        for (TodoExcelColumnEnum columnEnum : TodoExcelColumnEnum.values()) {
            StandardExcelCell cell = new StandardExcelCell();
            cell.setColumnNo(columnEnum.getIndex());
            cell.setExcelRow(toRow);
            Builder<String> valueBuilder = ExcelCellBuilderFactory.newInstance(columnEnum, srcRow);
            String cellValue = null != valueBuilder ? valueBuilder.build() : null;
            cell.setValue(cellValue);
            cell.setDescriptor(new StandardExcelCellDescriptor());
            cells.add(cell);
        }
        return cells;
    }

    private Excel obtainExcel() {
        return new StandardExcel();
    }

}
