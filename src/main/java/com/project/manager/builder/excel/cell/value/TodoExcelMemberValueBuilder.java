package com.project.manager.builder.excel.cell.value;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.project.manager.entity.po.RequirementScheduleItem;
import com.project.manager.entity.vo.Range;
import com.project.manager.enums.DailyExcelColumnEnum;
import com.project.manager.enums.TodoExcelColumnEnum;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.util.DateUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 经办人列值构造器
 * <p>
 * 经办人定义：明日所在时间区间内，用时岗位的相关参与者都是经办人
 * <p>
 * 多个经办人中需要取出当前需求状态对应的经办人
 *
 * @author zhaohui
 */
public class TodoExcelMemberValueBuilder extends AbstractTodoCellValueBuilder {

    private static final Pattern PATTERN_EXPECT = Pattern.compile("^(预计\\d{4})(周内)?.*$");

    private static final Pattern PATTERN_DAY_CERTAIN = Pattern.compile("^(\\d{4})\\s?(：|:)?\\s?.*$");

    private static final Pattern PATTERN_DAY_RANGE_STANDARD = Pattern.compile("^.*(\\d{4}-)?(\\d{4})$");

    private static final Pattern PATTERN_DAY_STANDARD = Pattern.compile("^(\\d{4}-)$");

    public TodoExcelMemberValueBuilder(ExcelRow dataRow) {
        super(dataRow);
    }


    @Override
    public TodoExcelColumnEnum column() {
        return TodoExcelColumnEnum.MEMBER;
    }

    @Override
    public String build() {
        // 根据排期状态获取经办人
        Set<String> members = Sets.newHashSet();
        members.addAll(getMemberFromReqScheduleStatus());
        members.addAll(getMemberFromSchedule());
        // 转成字符串
        StringBuilder sf = new StringBuilder();
        for (String mem : members) {
            sf.append("@").append(mem).append(";");
        }
        if (sf.length() > 0) {
            sf.delete(sf.length() - 1, sf.length());
        }
        return sf.toString();
    }

    private Set<String> getMemberFromSchedule() {
        ExcelCell reqScheduleCell = dataRow.getCellByName(DailyExcelColumnEnum.REQUIREMENT_SCHEDULE.getName());
        if (null == reqScheduleCell || StringUtils.isBlank(reqScheduleCell.getValue())) {
            return Sets.newHashSet();
        }
        List<RequirementScheduleItem> items = buildReqScheduleItems(reqScheduleCell.getValue());

        // 过滤含待办日的排期
        String todoDay = getTodoDay();
        items = filterScheduleWithinDateRange(items, DateUtil.parse(todoDay, "yyyy-MM-dd"));
        if (CollectionUtils.isEmpty(items)) {
            return Sets.newHashSet();
        }

        // 从排期安排中找到部门经办人。key-part;value-mem
        Map<String, String> memberMap = getMemberPartMap();

        // 从需求中找到部门经办人
        Set<String> members = Sets.newHashSet();
        for (RequirementScheduleItem item : items) {
            // 排期部门信息
            for (String part : item.getPart()) {
                // 获取部门中的人
                String member = memberMap.get(part);
                if (StringUtils.isNotBlank(member)) {
                    members.add(member);
                }
            }
        }
        return members;
    }

    /**
     * 根据需求状态获取相关部门
     *
     * @return parts
     */
    private List<String> getMemberFromReqScheduleStatus() {
        String status = dataRow.valueOfCellAt(DailyExcelColumnEnum.REQUIREMENT_STATUS.getIndex());
        List<String> parts = getPartsFromScheduleStatus(status);

        List<String> members = Lists.newArrayList();

        Map<String, String> memberPartMap = getMemberPartMap();
        for (String part : parts) {
            String mem = memberPartMap.get(part);
            if ("项管".equals(part)) {
                mem = "张倩";
            }
            if (StringUtils.isNotBlank(mem)) {
                members.add(mem);
            }
        }
        return members;
    }

    private List<RequirementScheduleItem> buildReqScheduleItems(String content) {
        String[] itemArr = content.split("\n");
        if (ArrayUtils.isEmpty(itemArr)) {
            return Lists.newArrayList();
        }
        List<RequirementScheduleItem> items = Lists.newArrayList();
        for (String item : itemArr) {
            List<RequirementScheduleItem> it = parseItem(item);
            if (null != it) {
                items.addAll(it);
            }
        }
        return items;
    }

    /**
     * 从数据行相关人列获取人员及部门map
     *
     * @return map key-part; value-mem
     */
    private Map<String, String> getMemberPartMap() {
        // 获取部门经办人
        Map<String, String> rsMap = Maps.newHashMap();
        String[] memberArray = dataRow.getCellByName(DailyExcelColumnEnum.MEMBER.getName()).getValue().split("\n");
        for (String standardMember : memberArray) {
            if (StringUtils.isBlank(standardMember)) {
                continue;
            }
            String[] stdMem = standardMember.split("：");
            String post = stdMem[0];
            String memName = stdMem[1];
            if (!rsMap.containsKey(post)) {
                rsMap.put(post, memName);
            }
        }
        return rsMap;
    }

    /**
     * 过滤排期项
     * <p>
     * 只保留未完成状态的并且待办日处于排期边界值的数据行
     *
     * @param items 排期项
     * @param date  待办日
     * @return 排期项
     */
    private List<RequirementScheduleItem> filterScheduleWithinDateRange(List<RequirementScheduleItem> items, Date date) {
        // 只保留未完成的排期
        List<RequirementScheduleItem> unfinishedItems = items.stream().filter(it -> !it.isDone()).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(unfinishedItems)) {
            return Lists.newArrayList();
        }
        // 过滤边界值
        List<RequirementScheduleItem> rs = Lists.newArrayList();
        for (RequirementScheduleItem item : unfinishedItems) {
            Range<String> dateRange = item.getDateRange();
            if (null == dateRange || StringUtils.isBlank(dateRange.getBegin()) || StringUtils.isBlank(dateRange.getEnd())) {
                continue;
            }
            boolean matchUpperBorder = DateUtil.isSameDay(date, DateUtil.parse(dateRange.getBegin()));
            boolean matchLowerBorder = DateUtil.isSameDay(date, DateUtil.parse(dateRange.getEnd()));
            boolean onBorder = matchUpperBorder || matchLowerBorder;
            if (onBorder) {
                rs.add(item);
            }
        }
        return rs;
    }

    /**
     * 解析排期条目
     * <p>
     * 排期条目由两部分组成：时间 + 事项
     * <p>
     * 时间两类：具体日期和范围日期
     * 具体日期有7种表述方式：
     * MMdd:
     * MMdd：
     * MMdd
     * 预计MMdd
     * 预计MMdd周内
     * ：MMdd-MMdd
     * ：MMdd
     * <p>
     * 事项通常有两部分组成: 岗位名称（可选）+工作简称。
     * 岗位名称包括：贷前、贷中、贷后、业务、FUND、数据中心、
     * 工作简称：开发、测试、验收、上线、产品配置
     *
     * @param itemStr 内容
     * @return 条目
     */
    private List<RequirementScheduleItem> parseItem(String itemStr) {
        if (StringUtils.isBlank(itemStr)) {
            return null;
        }
        int reqItemType = getRequirementItemType(itemStr);
        if (0 == reqItemType) {
            // 日志类型
            return parseLogTypeSchedule(itemStr);
        } else if (1 == reqItemType) {
            // 排期类型
            return parseReqTypeSchedule(itemStr);
        } else {
            System.out.println("未知的排期类型");
            return null;
        }
    }

    /**
     * 解析排期类型的排期条目
     * <p>
     * 特征：以日期结束
     *
     * @param itemStr
     * @return
     */
    private List<RequirementScheduleItem> parseReqTypeSchedule(String itemStr) {
        if (!isEndWithDate(itemStr)) {
            return null;
        }
        // 确定分隔符位置
        int splitIndex = itemStr.contains(":") ? itemStr.indexOf(":") : itemStr.indexOf("：");
        if (splitIndex == -1) {
            return null;
        }

        // 截取日期和文案
        String dateStr = itemStr.substring(splitIndex);
        String content = itemStr.substring(0, splitIndex);

        // 将日期转成标准格式 MMdd-MMdd
        dateStr = delExtraCharForDateStr(dateStr);

        List<Range<String>> dateRange = Lists.newArrayList();
        if (StringUtils.trim(dateStr).length() == 19) {
            // 两种情况： MMdd-MMdd&MMdd-MMdd || MMdd-MMdd&MMdd-MMdd
            if (dateStr.contains("&")) {
                String[] dateRangeArray = dateStr.split("&");
                String leftRange = dateRangeArray[0];
                String rightRange = dateRangeArray[1];
                dateRange.add(getDate(leftRange));
                dateRange.add(getDate(rightRange));
            } else if (dateStr.contains(" ")) {
                String[] dateRangeArray = dateStr.split(" ");
                String rightRange = dateRangeArray[1];
                dateRange.add(getDate(rightRange));
            } else {
                System.err.println("日期分隔符错误。dateStr = " + dateStr);
            }
        } else if (StringUtils.trim(dateStr).length() == 14) {
            // 4种情况：前9后4 或 前4后9,分隔符为&或 " "
            if (dateStr.contains("&")) {
                String[] dateArray = dateStr.split("&");
                if (dateArray.length != 2) {
                    System.err.println("分隔符异常，分隔符有且仅有一个。dateStr = " + dateStr);
                    return null;
                }
                String first = dateArray[0];
                String last = dateArray[1];
                if (StringUtils.length(first) == 4) {
                    first = first + "-" + first;
                    dateRange.add(getDate(first));
                } else {
                    last = last + "-" + last;
                    dateRange.add(getDate(last));
                }
            } else if (dateStr.contains(" ")) {
                String[] dateArray = dateStr.split(" ");
                if (dateArray.length != 2) {
                    System.err.println("分隔符异常，分隔符有且仅有一个。dateStr = " + dateStr);
                    return null;
                }
                String date = dateArray[1];
                if (StringUtils.length(date) == 4) {
                    date = date + "-" + date;
                }
                dateRange.add(getDate(date));
            } else {
                System.err.println("日期分隔符错误。dateStr = " + dateStr);
                return null;
            }
        } else if (StringUtils.trim(dateStr).length() == 9) {
            // 三种情况：MMdd-MMdd或MMdd MMdd或MMdd&MMdd
            if (dateStr.contains(" ")) {
                // 保留最后一个
                String[] dateArray = dateStr.split(" ");
                dateRange.add(getDate(dateArray[1] + "-" + dateArray[1]));
            } else if (dateStr.contains("-")) {
                dateRange.add(getDate(dateStr));
            } else if (dateStr.contains("&")) {
                String[] dateArray = dateStr.split("&");
                dateRange.add(getDate(dateArray[0] + "-" + dateArray[0]));
                dateRange.add(getDate(dateArray[1] + "-" + dateArray[1]));
            }
        } else if (StringUtils.trim(dateStr).length() == 4) {
            dateRange.add(getDate(dateStr + "-" + dateStr));
        } else {
            System.err.println("日期格式错误。dateStr = " + dateStr);
            return null;
        }
        List<String> partFromReqItem = getPartsFromSchedule(content);

        List<RequirementScheduleItem> rs = Lists.newArrayList();
        for (Range<String> range : dateRange) {
            rs.add(RequirementScheduleItem
                    .builder()
                    .isDone(isDone(range))
                    .part(partFromReqItem)
                    .dateRange(range)
                    .build()
            );
        }
        return rs;
    }

    private String delExtraCharForDateStr(String dateStr) {
        return dateStr
                .replaceAll(":", "")
                .replaceAll("：", "")
                .replaceAll(" -", "-")
                .replaceAll("- ", "-")
                .replaceAll(" &", "&")
                .replaceAll("& ", "&");
    }

    private Range<String> getDate(String dateStr) {
        if (StringUtils.isBlank(dateStr)) {
            return null;
        }
        String[] dates = dateStr.split("-");
        String lDate = dates[0];
        String hDate = dates[1];
        int lMonth = lDate.startsWith("0") ? Integer.parseInt(lDate.substring(1, 2)) : Integer.parseInt(lDate.substring(0, 2));
        int rMonth = hDate.startsWith("0") ? Integer.parseInt(hDate.substring(1, 2)) : Integer.parseInt(hDate.substring(0, 2));

        DateTime now = DateUtil.now();
        int monthOfYear = now.getMonthOfYear();
        int year = now.getYear();

        int minMonth = Math.max(1, monthOfYear - 3);
        int maxMonth = Math.min(12, monthOfYear + 3);

        int lYear = year;
        if (lMonth > maxMonth) {
            lYear = year + 1;
        } else if (lMonth < minMonth) {
            lYear = year - 1;
        }
        int rYear = rMonth < lMonth ? lYear + 1 : lYear;


        String beginDate = DateUtil.format(DateUtil.getBeginTimeOfDay(lYear + "-" + dates[0].substring(0, 2) + "-" + dates[0].substring(2, 4)), "yyyy-MM-dd HH:mm:dd");
        String endDate = DateUtil.format(DateUtil.getBeginTimeOfDay(rYear + "-" + dates[1].substring(0, 2) + "-" + dates[1].substring(2, 4)), "yyyy-MM-dd HH:mm:dd");

        return Range.<String>builder().begin(beginDate).end(endDate).build();
    }

    /**
     * 日志类型的排期特征：时间开头
     *
     * @param item 排期条目
     * @return item list
     */
    private List<RequirementScheduleItem> parseLogTypeSchedule(String item) {
        if (StringUtils.isBlank(item) || item.length() < 5) {
            return null;
        }
        DateTime now = DateUtil.now();
        int curMonth = now.getMonthOfYear();
        // 今年排期时间范围
        int curMinMonth = Math.max(1, curMonth - 3);
        int curMaxMonth = curMonth + 3;
        String itemDateStr = isStartWithExpectFixDate(item) ? item.substring(2, 6) : item.substring(0, 4);
        int itemMonth = itemDateStr.startsWith("0") ? Integer.parseInt(itemDateStr.substring(1, 2)) : Integer.parseInt(itemDateStr.substring(0, 2));
        int year = now.getYear();
        if (itemMonth > curMaxMonth) {
            year = year - 1;
        } else if (itemMonth < curMinMonth) {
            year = year + 1;
        }
        String day = itemDateStr.substring(2, 4);

        String dateStr = year + "-" + itemDateStr.substring(0, 2) + "-" + day;
        String beginDate = DateUtil.format(DateUtil.getBeginTimeOfDay(dateStr), "yyyy-MM-dd HH:mm:dd");
        String endDate = DateUtil.format(DateUtil.getEndTimeOfDay(dateStr), "yyyy-MM-dd HH:mm:dd");
        int contentStart = itemDateStr.length();
        String content = item.substring(contentStart);
        if (':' == (item.charAt(contentStart)) || '：' == (item.charAt(contentStart))) {
            content = item.substring(contentStart + 1);
        }
        Range<String> dateRange = Range.<String>builder().begin(beginDate).end(endDate).build();
        return Lists.newArrayList(RequirementScheduleItem.builder()
                .isDone(isDone(dateRange))
                .dateRange(dateRange)
                .part(getPartsFromSchedule(content))
                .build());
    }

    /**
     * 需求条目两种类型：日志类型和排期类型
     * <p>
     * 0,日志类型以日期描述开头并以日志内容结尾
     * 1,排期类型以工作内容开头并以时间范围结尾
     *
     * @param item
     * @return
     */
    private int getRequirementItemType(String item) {
        if (isStartWithDate(item)) {
            return 0;
        } else if (isEndWithDate(item)) {
            return 1;
        } else {
            return -1;
        }

    }

    private boolean isEndWithDate(String item) {
        return StringUtils.isNotBlank(item) && item.length() > 4 && NumberUtils.isDigits(item.substring(item.length() - 4));
    }

    /**
     * 时间模板有4种，符合任意一种即返回true
     * <p>
     * 排期中的时间格式有4中：
     * 固定时间, 0125|0125:|0125：
     * 固定时间范围，
     * 预期固定时间
     * 预期时间范围
     *
     * @param item 排期条目
     * @return bool
     */
    private boolean isStartWithDate(String item) {
        return isStartWithFixDate(item) || isStartWithExpectFixDate(item);
    }

    private boolean isStartWithFixDate(String item) {
        return StringUtils.isNotBlank(item) && item.length() > 4 && NumberUtils.isDigits(item.substring(0, 4));
    }

    private boolean isStartWithExpectFixDate(String item) {
        return StringUtils.isNotBlank(item) && item.length() > 6 && "预计".equals(item.substring(0, 2)) && NumberUtils.isDigits(item.substring(2, 6));
    }

    private boolean isDone(Range<String> dateRange) {
        String status = dataRow.valueOfCellAt(DailyExcelColumnEnum.REQUIREMENT_STATUS.getIndex());
        boolean isTodoStatus = status.contains("待");
        String todoDay = getTodoDay();
        return isTodoStatus
                && todoDay.replaceAll("-", "").compareTo(dateRange.getEnd().replaceAll("-", "")) > 0;
    }

}
