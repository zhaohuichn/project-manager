package com.project.manager.builder.excel.cell.value;

import com.project.manager.ConfigService;
import com.project.manager.enums.DailyExcelColumnEnum;
import com.project.manager.enums.ProjectStatusMapExcelColumnEnum;
import com.project.manager.enums.TodoExcelColumnEnum;
import com.project.manager.parse.excel.StandardExcelParser;
import com.project.manager.source.excel.Excel;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.source.excel.row.StandardRow;
import com.project.manager.util.ExcelUtil;
import com.project.manager.util.SpringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.File;
import java.util.List;

/**
 * 待办类型列值构造器
 * <p>
 * 待办类型依赖于需求状态及需求排期
 * “待”字开头且日期为明天时，待办类型为”确认“
 * 流程周期内切距离截止时间小于等于阈值时，待办类型为”提醒“。阈值暂定为1
 *
 * @author zhaohui
 */
public class TodoExcelTypeValueBuilder extends AbstractTodoCellValueBuilder {

    public TodoExcelTypeValueBuilder(ExcelRow dataRow) {
        super(dataRow);
    }


    @Override
    public TodoExcelColumnEnum column() {
        return TodoExcelColumnEnum.TODO_TYPE;
    }

    @Override
    public String build() {
        // 数据列状态
        ExcelCell statusCell = dataRow.getCellByName(DailyExcelColumnEnum.REQUIREMENT_STATUS.getName());
        if (null == statusCell || StringUtils.isBlank(statusCell.getValue())) {
            return null;
        }
        // 根据规则直接解析出todo类型
        ConfigService configService = SpringUtil.getBean(ConfigService.class);
        Excel configExcel = new StandardExcelParser().parse(new File(configService.getConfig("configPath")));
        List<ExcelRow> dataRows = configExcel.getSheetByName("需求状态&代办类型映射").getDataRows();
        ExcelCell typeCell = dataRows.stream()
                .filter(row -> statusCell.getValue().equals(row.cellAt(ProjectStatusMapExcelColumnEnum.REQUIREMENT_STATUS.getIndex()).getValue()))
                .findFirst().orElse(new StandardRow())
                .cellAt(ProjectStatusMapExcelColumnEnum.TODO_TYPE.getIndex());
        if (null == typeCell) {
            return null;
        }
        return typeCell.getValue();
    }

}
