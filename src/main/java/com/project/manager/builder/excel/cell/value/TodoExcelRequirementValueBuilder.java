package com.project.manager.builder.excel.cell.value;

import com.project.manager.enums.DailyExcelColumnEnum;
import com.project.manager.enums.TodoExcelColumnEnum;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import org.apache.poi.sl.draw.geom.GuideIf;

import java.util.Optional;

/**
 * 需求名称列值构造器
 *
 * @author zhaohui
 */
public class TodoExcelRequirementValueBuilder extends AbstractTodoCellValueBuilder {

    public TodoExcelRequirementValueBuilder(ExcelRow dataRow) {
        super(dataRow);
    }

    @Override
    public String build() {
        // 子序号
        ExcelCell subReqNoCell = dataRow.getCellByName(DailyExcelColumnEnum.REQUIREMENT_NO.getName());
        String subNo = null != subReqNoCell ? Optional.ofNullable(subReqNoCell.getValue()).orElse("") : "";
        // 需求名称
        ExcelCell reqNameCell = dataRow.getCellByName(DailyExcelColumnEnum.REQUIREMENT_NAME.getName());
        String reqName = null != reqNameCell ? Optional.ofNullable(reqNameCell.getValue()).orElse("") : "";
        return subNo + " " + reqName;
    }

    @Override
    public TodoExcelColumnEnum column() {
        return TodoExcelColumnEnum.REQUIREMENT;
    }

}
