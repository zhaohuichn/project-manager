package com.project.manager.builder.excel.cell.value;

import com.project.manager.enums.DailyExcelColumnEnum;
import com.project.manager.enums.TodoExcelColumnEnum;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.row.ExcelRow;

/**
 * 需求文档列值构造器
 * <p>
 *
 * @author zhaohui
 */
public class TodoExcelDocValueBuilder extends AbstractTodoCellValueBuilder {

    public TodoExcelDocValueBuilder(ExcelRow dataRow) {
        super(dataRow);
    }

    @Override
    public String build() {
        ExcelCell cell = dataRow.cellAt(DailyExcelColumnEnum.DOCUMENT_URL.getIndex());
        return null != cell ? cell.getValue() : null;
    }

    @Override
    public TodoExcelColumnEnum column() {
        return TodoExcelColumnEnum.DOCUMENT_URL;
    }

}
