package com.project.manager.builder.excel.cell.value;

import com.project.manager.enums.DailyExcelColumnEnum;
import com.project.manager.enums.TodoExcelColumnEnum;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.row.ExcelRow;

/**
 * 所属模块列值构造器
 * <p>
 * 每个行都有所属分组字段，直接从模块取值即可
 *
 * @author zhaohui
 */
public class TodoExcelModuleValueBuilder extends AbstractTodoCellValueBuilder {

    public TodoExcelModuleValueBuilder(ExcelRow dataRow) {
        super(dataRow);
    }

    @Override
    public String build() {
        ExcelCell cell = dataRow.getCellByName(DailyExcelColumnEnum.MODULE.getName());
        return null != cell ? cell.getValue() : "";
    }

    @Override
    public TodoExcelColumnEnum column() {
        return TodoExcelColumnEnum.MODULE;
    }

}
