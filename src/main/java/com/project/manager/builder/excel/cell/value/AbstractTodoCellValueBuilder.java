package com.project.manager.builder.excel.cell.value;

import com.google.common.collect.Lists;
import com.project.manager.ConfigService;
import com.project.manager.configuration.PartConfiguration;
import com.project.manager.configuration.ReqStatusPartConfiguration;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.util.DateUtil;
import com.project.manager.util.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * AbstractTodoCellValueBuilder
 *
 * @author ZHAOHUI
 */
@Slf4j
public abstract class AbstractTodoCellValueBuilder implements TodoCellValueBuilder {

    /**
     * 数据行
     */
    protected ExcelRow dataRow;

    public AbstractTodoCellValueBuilder(ExcelRow dataRow) {
        this.dataRow = dataRow;
    }

    protected String getTodoDay() {
        ConfigService configService = SpringUtil.getBean(ConfigService.class);
        String todoDayConfig = configService.getConfig("todoDay");
        if ("today".equals(todoDayConfig)) {
            return DateUtil.format(DateUtil.now().toDate(), "yyyy-MM-dd");
        } else {
            return DateUtil.format(DateUtil.tomorrow(), "yyyy-MM-dd");

        }
    }

    /**
     * 从排期内容中解析出关联的部门
     *
     * @param content content
     * @return parts
     */
    protected List<String> getPartsFromSchedule(String content) {
        if (StringUtils.isEmpty(content)) {
            return new ArrayList<>();
        }
        // 关键字
        List<String> rs = Lists.newArrayList();
        ConfigService configService = SpringUtil.getBean(ConfigService.class);
        PartConfiguration partConf = configService.getPartConfiguration();
        for (String kw : partConf.keySet()) {
            if (content.contains(kw)) {
                List<String> memberParts = partConf.getOrDefault(kw, Lists.newArrayList());
                rs.addAll(memberParts);
            }
        }
        return rs;
    }

    protected List<String> getPartsFromScheduleStatus(String status) {
        if (StringUtils.isBlank(status)) {
            return new ArrayList<>();
        }
        List<String> rs = Lists.newArrayList();
        ConfigService configService = SpringUtil.getBean(ConfigService.class);
        ReqStatusPartConfiguration partConf = configService.getReqStatusPartConfiguration();
        for (String kw : partConf.keySet()) {
            if (status.contains(kw)) {
                List<String> memberParts = partConf.getOrDefault(kw, Lists.newArrayList());
                rs.addAll(memberParts);
            }
        }
        return rs;
    }

}
