package com.project.manager.builder.excel.cell.value;

import com.project.manager.builder.Builder;
import com.project.manager.enums.TodoExcelColumnEnum;

/**
 * 待办事项列值构造器
 *
 * @author ZHAOHUI
 */
public interface TodoCellValueBuilder extends Builder<String> {

    /**
     * 单元格所属列
     *
     * @return col
     */
    TodoExcelColumnEnum column();

}
