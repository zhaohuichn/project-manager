package com.project.manager.builder.excel.cell.value;

import com.project.manager.ConfigService;
import com.project.manager.enums.DailyExcelColumnEnum;
import com.project.manager.enums.ProjectStatusMapExcelColumnEnum;
import com.project.manager.enums.TodoExcelColumnEnum;
import com.project.manager.parse.excel.StandardExcelParser;
import com.project.manager.source.excel.Excel;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.source.excel.row.StandardRow;
import com.project.manager.util.ExcelUtil;
import com.project.manager.util.SpringUtil;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;

/**
 * 待办事项内容构造器
 * <p>
 * 从配置文件中读取文案模板
 * 解析模板中的变量并自动填充
 * 填充内容根据需求状态、需求排期和人员安排决定
 *
 * @author zhaohui
 */
public class TodoExcelContentValueBuilder extends AbstractTodoCellValueBuilder {

    public TodoExcelContentValueBuilder(ExcelRow dataRow) {
        super(dataRow);
    }

    @Override
    public String build() {
        // 当前需求状态
        ExcelCell statusCell = dataRow.getCellByName(DailyExcelColumnEnum.REQUIREMENT_STATUS.getName());
        if (null == statusCell || StringUtils.isBlank(statusCell.getValue())) {
            return null;
        }
        String status = statusCell.getValue();

        // 文案配置
        ConfigService configService = SpringUtil.getBean(ConfigService.class);
        Excel configExcel = new StandardExcelParser().parse(new File(configService.getConfig("configPath")));
        List<ExcelRow> dataRows = configExcel.getSheetByName("需求状态&代办类型映射").getDataRows();
        ExcelCell templateCell = dataRows.stream()
                .filter(dr -> status.equals(dr.cellAt(ProjectStatusMapExcelColumnEnum.REQUIREMENT_STATUS.getIndex()).getValue()))
                .findFirst().orElse(new StandardRow())
                .cellAt(ProjectStatusMapExcelColumnEnum.TODO_CONTENT_TEMPLATE.getIndex());
        if (null == templateCell) {
            return null;
        }
        String contentTemplate = templateCell.getValue();
        // 模板变量填充
        return paddingVariable(contentTemplate, status, dataRow);
    }

    private String paddingVariable(String contentTemplate, String status, ExcelRow dataRow) {
        return contentTemplate;
    }


    @Override
    public TodoExcelColumnEnum column() {
        return TodoExcelColumnEnum.TODO_CONTENT;
    }
}
