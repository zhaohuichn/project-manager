package com.project.manager.builder;

/**
 * 构造器
 *
 * @author ZHAOHUI
 */
public interface Builder<T> {

    T build();

}
