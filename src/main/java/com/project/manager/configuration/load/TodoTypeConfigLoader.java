package com.project.manager.configuration.load;

import com.project.manager.enums.ConfigSheetEnum;
import com.project.manager.enums.TodoSheetColumnEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * TodoTypeConfigLoader
 *
 * @author ZHAOHUI
 */
@Slf4j
public class TodoTypeConfigLoader extends AbstractTodoExcelConfigLoader {


    @Override
    public ConfigSheetEnum sheet() {
        return ConfigSheetEnum.TODO_TYPE;
    }

    @Override
    public TodoSheetColumnEnum keyCol() {
        return TodoSheetColumnEnum.REQ_STATUS;
    }

    @Override
    public TodoSheetColumnEnum valueCol() {
        return TodoSheetColumnEnum.TODO_TYPE;
    }
}
