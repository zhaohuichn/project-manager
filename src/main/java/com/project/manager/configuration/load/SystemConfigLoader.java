package com.project.manager.configuration.load;

/**
 * SystemConfigLoader
 *
 * @author ZHAOHUI
 */
public class SystemConfigLoader extends AbstractConfigLoader {
    @Override
    protected void refreshProperties() {
        cachedProps.clear();
        cachedProps.putAll(System.getProperties());
    }
}
