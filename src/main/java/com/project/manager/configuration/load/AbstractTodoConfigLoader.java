package com.project.manager.configuration.load;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * AbstractTodoConfigLoader
 *
 * @author ZHAOHUI
 */
@Slf4j
public abstract class AbstractTodoConfigLoader extends AbstractConfigLoader {

    protected String configLocation() {
        String filePath = "/application.properties";
        try (InputStream is = new ClassPathResource(filePath).getInputStream()) {
            Properties props = new Properties();
            props.load(is);
            return props.getProperty("configPath");
        } catch (IOException e) {
            log.error("配置文件读取异常。path = " + filePath, e);
            return null;
        }
    }

}
