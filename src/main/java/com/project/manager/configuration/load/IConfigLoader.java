package com.project.manager.configuration.load;

import java.util.Properties;

/**
 * IConfigLoader
 *
 * @author ZHAOHUI
 */
public interface IConfigLoader {

    Properties load();

}
