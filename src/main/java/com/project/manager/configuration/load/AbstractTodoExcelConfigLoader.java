package com.project.manager.configuration.load;

import com.project.manager.enums.ConfigSheetEnum;
import com.project.manager.enums.TodoSheetColumnEnum;
import com.project.manager.parse.excel.StandardExcelParser;
import com.project.manager.source.excel.Excel;
import com.project.manager.source.excel.sheet.ExcelSheet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * AbstractTodoConfigLoader
 *
 * @author ZHAOHUI
 */
@Slf4j
public abstract class AbstractTodoExcelConfigLoader extends AbstractTodoConfigLoader {


    public abstract ConfigSheetEnum sheet();

    public abstract TodoSheetColumnEnum keyCol();

    public abstract TodoSheetColumnEnum valueCol();

    @Override
    protected void refreshProperties() {
        cachedProps.clear();


        String configPath = configLocation();

        Excel configExcel = new StandardExcelParser().parse(new File(configPath));
        ExcelSheet sheet = configExcel.getSheet(sheet().getIndex());
        if (null == sheet || CollectionUtils.isEmpty(sheet.getDataRows())) {
            log.info("Sheet is not exists or data rows is empty! file path = {}, sheet is = {}", configPath, sheet());
            return;
        }

        sheet.getDataRows().forEach(r -> {
            String key = r.cellAt(keyCol().getIndex()).getValue();
            String value = r.cellAt(valueCol().getIndex()).getValue();
            cachedProps.put(key, value);
        });

    }


}
