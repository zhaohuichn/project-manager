package com.project.manager.configuration.load;

import org.springframework.beans.factory.InitializingBean;

import java.util.Properties;

/**
 * 配置文件加载器抽象父类
 *
 * @author ZHAOHUI
 */
public abstract class AbstractConfigLoader implements IConfigLoader {

    /**
     * 缓存已经加载的配置
     */
    protected Properties cachedProps;

    protected boolean needRefresh = false;

    @Override
    public Properties load() {
        if (null != cachedProps && !needRefresh) {
            return cachedProps;
        }
        ensureCachePropsInitialized();
        if (needRefresh) {
            refreshProperties();
        }
        return cachedProps;
    }

    protected abstract void refreshProperties();

    private void ensureCachePropsInitialized() {
        if (null != this.cachedProps) {
            return;
        }
        // 双检锁保证缓存初始化
        synchronized (this) {
            if (null == this.cachedProps) {
                this.cachedProps = new Properties();
            }
        }
    }


}
