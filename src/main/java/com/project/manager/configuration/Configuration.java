package com.project.manager.configuration;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 配置信息
 *
 * @author zhaohui
 */
public class Configuration extends ConcurrentHashMap<String, String> {

    public Configuration() {
        super(16);
    }

}
