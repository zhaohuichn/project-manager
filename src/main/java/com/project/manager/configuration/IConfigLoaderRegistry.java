package com.project.manager.configuration;

import com.project.manager.configuration.load.IConfigLoader;

import java.util.List;

/**
 * 注册中心
 *
 * @author ZHAOHUI
 */
public interface IConfigLoaderRegistry {

    void register(String key, IConfigLoader loader);

    IConfigLoader get(String String);

    IConfigLoader get(Class<? extends IConfigLoader> clazz);

    List<IConfigLoader> getValues();
}
