package com.project.manager.configuration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.project.manager.configuration.load.IConfigLoader;
import com.project.manager.configuration.load.SystemConfigLoader;
import com.project.manager.configuration.load.TodoTypeConfigLoader;

import java.util.List;
import java.util.Map;

/**
 * 配置加载器注册中心
 *
 * @author ZHAOHUI
 */

public class ConfigLoaderIConfigLoaderRegistry implements IConfigLoaderRegistry {

    private final Map<String, IConfigLoader> registeredConfigLoader = Maps.newConcurrentMap();

    public ConfigLoaderIConfigLoaderRegistry() {
        register("todoTypeConfigLoader", new TodoTypeConfigLoader());
        register("systemConfigLoader", new SystemConfigLoader());
    }


    @Override
    public void register(String name, IConfigLoader loader) {
        if (null != loader) {
            registeredConfigLoader.put(name, loader);
        }
    }

    @Override
    public IConfigLoader get(String name) {
        return registeredConfigLoader.get(name);
    }

    @Override
    public IConfigLoader get(Class<? extends IConfigLoader> clazz) {
        for (IConfigLoader loader : registeredConfigLoader.values()) {
            if (loader.getClass().equals(clazz)) {
                return loader;
            }
        }
        return null;
    }

    @Override
    public List<IConfigLoader> getValues() {
        return Lists.newArrayList(registeredConfigLoader.values());
    }
}
