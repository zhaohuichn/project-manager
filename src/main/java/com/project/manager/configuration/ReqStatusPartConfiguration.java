package com.project.manager.configuration;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * PartConfiuration
 *
 * @author zhaohui
 */
public class ReqStatusPartConfiguration extends ConcurrentHashMap<String, List<String>> {

    public ReqStatusPartConfiguration() {
        super(64);
    }

    public List<String> getByKeyword(String kw) {
        return getOrDefault(kw, Lists.newArrayList());
    }

}
