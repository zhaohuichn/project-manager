package com.project.manager.source.excel.row;

import com.project.manager.source.excel.Excel;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.sheet.ExcelSheet;

import java.util.List;

/**
 * SheetRow
 *
 * @author ZHAOHUI
 */
public interface ExcelRow {

    /**
     * 所属 excel
     *
     * @return excel
     */
    Excel getExcel();

    /**
     * 所属页签
     *
     * @return sheet
     */
    ExcelSheet getSheet();

    void setSheet(ExcelSheet sheet);

    /**
     * 行号
     *
     * @return row no
     */
    int getRowNo();

    void setRowNo(int rowNo);

    /**
     * 是否表头
     *
     * @return bool
     */
    boolean isHeader();

    /**
     * 单元格
     *
     * @return cell list
     */
    List<ExcelCell> getCells();

    /**
     * 根据索引查询单元格
     *
     * @param pos 索引
     * @return cell
     */
    ExcelCell cellAt(int pos);

    /**
     * 根据列名获取单元格
     *
     * @param column 列名
     * @return cell
     */
    ExcelCell getCellByName(String column);

    /**
     * 指定单元格的值
     *
     * @param pos
     * @return
     */
    String valueOfCellAt(int pos);

}
