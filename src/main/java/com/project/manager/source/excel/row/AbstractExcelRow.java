package com.project.manager.source.excel.row;

import com.google.common.collect.Lists;
import com.project.manager.source.excel.Excel;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.sheet.ExcelSheet;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Optional;

/**
 * StandardRow
 *
 * @author ZHAOHUI
 */
public abstract class AbstractExcelRow implements ExcelRow {

    protected int rowNo;

    protected ExcelSheet sheet;

    protected boolean header;

    protected List<ExcelCell> cells;

    @Override
    public int getRowNo() {
        return rowNo;
    }

    @Override
    public void setRowNo(int rowNo) {
        this.rowNo = rowNo;
    }

    @Override
    public ExcelSheet getSheet() {
        return sheet;
    }

    @Override
    public void setSheet(ExcelSheet sheet) {
        this.sheet = sheet;
    }

    @Override
    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    @Override
    public List<ExcelCell> getCells() {
        return cells;
    }

    public void setCells(List<ExcelCell> cells) {
        this.cells = cells;
    }

    @Override
    public Excel getExcel() {
        return sheet.getExcel();
    }

    @Override
    public ExcelCell cellAt(int pos) {
        return CollectionUtils.size(cells) >= (pos + 1) ? cells.get(pos) : null;
    }

    @Override
    public ExcelCell getCellByName(String column) {
        return Optional.ofNullable(cells).orElse(Lists.newArrayList()).stream()
                .filter(c -> c.getColumnName().equals(column))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String valueOfCellAt(int pos) {
        ExcelCell cell = cellAt(pos);
        if (null == cell) {
            return "";
        }
        return Optional.ofNullable(cell.getValue()).orElse("");
    }
}
