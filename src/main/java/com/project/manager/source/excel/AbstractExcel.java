package com.project.manager.source.excel;

import com.google.common.collect.Lists;
import com.project.manager.source.AbstractSource;
import com.project.manager.source.excel.sheet.ExcelSheet;
import com.project.manager.util.SourceUtil;
import org.apache.commons.collections4.CollectionUtils;

import javax.swing.text.html.Option;
import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * TodoExcel
 *
 * @author zhaohui
 */
public abstract class AbstractExcel extends AbstractSource implements Excel {

    private String name;

    private List<ExcelSheet> sheets;

    public AbstractExcel() {
        this.sheets = Lists.newArrayList();
    }

    public AbstractExcel(String path) {
        super(path);
        sheets = Lists.newArrayList();
    }

    @Override
    public String name() {
        return new File(abstractPath).getName();
    }

    @Override
    public URL url() {
        return SourceUtil.getUrl(abstractPath);
    }

    @Override
    public ExcelSheet getSheet(int index) {
        return sheets.get(index);
    }

    @Override
    public ExcelSheet getSheetByName(String sheetName) {
        return Optional.ofNullable(sheets).orElse(Lists.newArrayList()).stream()
                .filter(st -> st.getName().equals(sheetName))
                .findFirst()
                .orElse(null);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<ExcelSheet> getSheets() {
        return sheets;
    }

    public void setSheets(List<ExcelSheet> sheets) {
        this.sheets = sheets;
    }

    public void addSheet(ExcelSheet sheet) {
        if (null == sheets) {
            sheets = Lists.newArrayList();
        }
        sheets.add(sheet);
    }
}
