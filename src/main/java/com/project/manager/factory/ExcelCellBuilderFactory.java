package com.project.manager.factory;

import com.project.manager.builder.*;
import com.project.manager.builder.excel.cell.value.*;
import com.project.manager.enums.TodoExcelColumnEnum;
import com.project.manager.source.excel.row.ExcelRow;

/**
 * ExcelCellBuilderFactory
 *
 * @author ZHAOHUI
 */
public class ExcelCellBuilderFactory {

    public static Builder<String> newInstance(TodoExcelColumnEnum todoCol, ExcelRow dataRow) {
        if (TodoExcelColumnEnum.MEMBER == todoCol) {
            return new TodoExcelMemberValueBuilder(dataRow);
        } else if (TodoExcelColumnEnum.MODULE == todoCol) {
            return new TodoExcelModuleValueBuilder(dataRow);
        } else if (TodoExcelColumnEnum.REQUIREMENT == todoCol) {
            return new TodoExcelRequirementValueBuilder(dataRow);
        } else if (TodoExcelColumnEnum.DOCUMENT_URL == todoCol) {
            return new TodoExcelDocValueBuilder(dataRow);
        } else if (TodoExcelColumnEnum.TODO_TYPE == todoCol) {
            return new TodoExcelTypeValueBuilder(dataRow);
        } else if (TodoExcelColumnEnum.TODO_CONTENT == todoCol) {
            return new TodoExcelContentValueBuilder(dataRow);
        } else {
            return null;
        }
    }
}
