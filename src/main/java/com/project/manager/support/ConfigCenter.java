package com.project.manager.support;

import com.project.manager.configuration.IConfigLoaderRegistry;
import com.project.manager.configuration.load.IConfigLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ServiceLoader;

/**
 * 配置中心
 * <p>
 * 配置中心将通过SPI的方式加载全局变量
 * <p>
 * 配置中心内部维护Properties对象，key必须为字符串类型
 *
 * @author ZHAOHUI
 */
@Slf4j
@Component("configCenter")
public class ConfigCenter {

    @Value("${configPath}")
    private String configPath;


    private Properties props;


    public ConfigCenter() {
    }

    @PostConstruct
    private void init() {
        log.info("Config center is initialize ...");
        props = new Properties();

        // 加载系统变量
        props.putAll(System.getProperties());

        // 加载配置文件路径
        props.put("configPath", configPath);

        //
        ServiceLoader<IConfigLoaderRegistry> configServices = ServiceLoader.load(IConfigLoaderRegistry.class);
        for (IConfigLoaderRegistry service : configServices) {
            List<IConfigLoader> values = service.getValues();
            for (IConfigLoader loader : values) {
                props.putAll(Optional.ofNullable(loader.load()).orElse(new Properties()));
            }
        }
        log.info("Config center is initialized!");
    }

    /**
     * 获取value值
     *
     * @param key key
     * @return value obj
     */
    public Object get(String key) {
        Object obj = props.get(key);
        if (null != obj) {
            return obj;
        }
        return props.get(key);
    }

    public String getProperty(String key) {
        return props.getProperty(key, "");
    }

}
