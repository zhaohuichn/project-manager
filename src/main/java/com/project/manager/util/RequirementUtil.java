package com.project.manager.util;


import com.google.common.collect.Lists;
import com.project.manager.constants.ProjectConstants;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Optional;

/**
 * 需求工具类
 *
 * @author ZHAOHUI
 */
public class RequirementUtil {
    
    public static List<String> getVariable(String str) {
        if (StringUtils.isBlank(str)) {
            return Lists.newArrayList();
        }
        List<String> var = Lists.newArrayList();
        String[] ss = str.split("\\$\\{");
        for (String s : ss) {
            int last = s.lastIndexOf("}");
            if (-1 < last) {
                var.add(s.substring(0, last));
            }
        }
        return var;
    }

}
