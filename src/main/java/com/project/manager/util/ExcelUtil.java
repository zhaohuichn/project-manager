package com.project.manager.util;

import com.google.common.collect.Lists;
import com.project.manager.source.descriptor.StandardExcelCellDescriptor;
import com.project.manager.source.excel.Excel;
import com.project.manager.source.excel.cell.ExcelCell;
import com.project.manager.source.excel.cell.StandardExcelCell;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.source.excel.row.StandardRow;
import com.project.manager.source.excel.sheet.ExcelSheet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * ExcelUtil
 *
 * @author ZHAOHUI
 */
@Slf4j
public class ExcelUtil {

    public static void main(String[] args) {
        InputStream is = ExcelUtil.class.getResourceAsStream("config.xlsx");
        try (FileOutputStream fos = new FileOutputStream("D:\\doc\\design\\test.xlsx")) {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("12");
            XSSFRow row = sheet.createRow(0);
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("test");

            workbook.write(fos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeWorkbook(Excel excel, String path) {
        try (FileOutputStream fos = new FileOutputStream(path)) {
            XSSFWorkbook workbook = getXSSFWorkbookFromExcel(excel);
            workbook.write(fos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static XSSFWorkbook getXSSFWorkbookFromExcel(Excel excel) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        if (null == excel || CollectionUtils.isEmpty(excel.getSheets())) {
            return workbook;
        }
        List<ExcelSheet> sheets = excel.getSheets();
        sheets.sort(Comparator.comparing(ExcelSheet::getIndex));
        for (ExcelSheet sheet : sheets) {
            XSSFSheet st = workbook.createSheet(sheet.getName());
            List<ExcelRow> rows = sheet.getRows();
            if (CollectionUtils.isEmpty(rows)) {
                continue;
            }
            rows.sort(Comparator.comparing(ExcelRow::getRowNo));
            // 构建sheet页
            for (ExcelRow row : rows) {
                XSSFRow xssfRow = st.createRow(st.getLastRowNum() + 1);
                if (CollectionUtils.isEmpty(row.getCells())) {
                    continue;
                }
                List<ExcelCell> cells = row.getCells();
                cells.sort(Comparator.comparing(ExcelCell::getColumnNo));
                // 构建行数据
                for (ExcelCell excelCell : cells) {
                    XSSFCell cell = xssfRow.createCell(excelCell.getColumnNo());
                    cell.setCellValue(excelCell.getValue());
                }
            }
        }
        return workbook;
    }


    public static XSSFWorkbook readWorkbook(InputStream inputStream) {
        try {
            log.info("input stream : {}", null != inputStream);
            return new XSSFWorkbook(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getCellStringValue(Cell cell) {
        if (null == cell) {
            return "";
        }
        if (CellType.NUMERIC == cell.getCellType()) {
            return String.valueOf(cell.getNumericCellValue());
        } else {
            return cell.getStringCellValue();
        }
    }


    public static ExcelRow buildSheetHeader(Map<Integer, String> columnMap) {
        if (MapUtils.isEmpty(columnMap)) {
            return null;
        }
        StandardRow row = new StandardRow();
        row.setRowNo(0);
        row.setHeader(true);
        List<ExcelCell> cells = Lists.newArrayList();
        for (Map.Entry<Integer, String> colEntry : columnMap.entrySet()) {
            StandardExcelCell cell = new StandardExcelCell();
            cell.setExcelRow(row);
            cell.setColumnNo(colEntry.getKey());
            cell.setValue(colEntry.getValue());
            cell.setDescriptor(new StandardExcelCellDescriptor());
            cells.add(cell);
        }
        row.setCells(cells);

        return row;
    }


}
