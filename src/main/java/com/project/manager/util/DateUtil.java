package com.project.manager.util;

import com.project.manager.entity.vo.Range;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Date;

/**
 * DateUtil
 *
 * @author ZHAOHUI
 */
public class DateUtil {


    public static DateTime now() {
        return new DateTime();
    }

    public static Date yesterday() {
        return new LocalDateTime().plusDays(-1).toDate();
    }

    public static Date tomorrow() {
        return new LocalDateTime().plusDays(1).toDate();
    }

    public static Date parse(String dateStr) {
        return parse(dateStr, "yyyy-MM-dd HH:mm:dd");
    }

    public static Date parse(String dateStr, String format) {
        return DateTime.parse(dateStr, DateTimeFormat.forPattern(format)).toDate();
    }

    public static String format(Date date, String pattern) {
        return new LocalDateTime(date).toString(pattern);
    }

    public static Date getBeginTimeOfDay(String dateStr) {
        return new LocalDateTime(dateStr).millisOfDay().withMinimumValue().toDate();
    }

    public static Date getBeginTimeOfDay(Date date) {
        return new LocalDateTime(date).millisOfDay().withMinimumValue().toDate();
    }

    public static Date getEndTimeOfDay(String dateStr) {
        return new LocalDateTime(dateStr).millisOfDay().withMaximumValue().toDate();
    }


    public static Date getEndTimeOfDay(Date date) {
        return new LocalDateTime(date).millisOfDay().withMaximumValue().toDate();
    }

    public static boolean isDayBetweenRange(Date day, Range<String> dateRange) {
        return isDayBetweenRange(day, parse(dateRange.getBegin()), parse(dateRange.getEnd()));
    }


    public static boolean isDayBetweenRange(Date day, Date begin, Date end) {
        Date d = getBeginTimeOfDay(day);
        Date bd = getBeginTimeOfDay(begin);
        Date ed = getBeginTimeOfDay(end);
        return d.compareTo(bd) >= 0 && d.compareTo(ed) <= 0;
    }

    public static boolean isSameDay(String d1, String d2) {
        return isSameDay(parse(d1), parse(d2));
    }

    public static boolean isSameDay(Date d1, Date d2) {
        return 0 == getBeginTimeOfDay(d1).compareTo(getBeginTimeOfDay(d2));
    }

    public static boolean equals(Date d1, Date d2, String format) {
        return format(d1, format).equals(format(d2, format));
    }

    public static void main(String[] args) {
        System.out.println(format(getBeginTimeOfDay(new Date()), "yyyy-MM-dd HH:mm:ss"));
        System.out.println(format(getEndTimeOfDay(new Date()), "yyyy-MM-dd HH:mm:ss"));
        System.out.println(getBeginTimeOfDay(parse("20210101")));
    }

}
