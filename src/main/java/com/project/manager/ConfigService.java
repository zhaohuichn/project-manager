package com.project.manager;

import com.project.manager.configuration.Configuration;
import com.project.manager.configuration.PartConfiguration;
import com.project.manager.configuration.ReqStatusPartConfiguration;
import com.project.manager.parse.PartParser;
import com.project.manager.parse.ReqStatusPartParser;
import com.project.manager.parse.excel.StandardExcelParser;
import com.project.manager.source.excel.Excel;
import com.project.manager.source.excel.row.ExcelRow;
import com.project.manager.source.excel.sheet.ExcelSheet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;

/**
 * ConfigService
 *
 * @author zhaohui
 */
@Service
public class ConfigService {

    private Configuration configuration;

    private PartConfiguration partConfiguration;

    private ReqStatusPartConfiguration reqStatusPartConfiguration;


    @Value("${configPath}")
    private String configPath;

    @PostConstruct
    private void init() {
        configuration = new Configuration();
        configuration.put("configPath", configPath);
        try {
            Excel configExcel = new StandardExcelParser().parse(new File(configPath));
            ExcelSheet locationSheet = configExcel.getSheetByName("sysConfig");
            List<ExcelRow> dataRows = locationSheet.getDataRows();
            dataRows.forEach(dr -> configuration.put(dr.getCellByName("key").getValue(), dr.getCellByName("value").getValue()));

            // 岗位映射
            ExcelSheet partConfigSheet = configExcel.getSheetByName("partConfig");
            partConfiguration = new PartParser().parse(partConfigSheet);

            // 需求状态岗位映射
            ExcelSheet reqStatusPartConfigSheet = configExcel.getSheetByName("reqStatusPartConfig");
            reqStatusPartConfiguration = new ReqStatusPartParser().parse(reqStatusPartConfigSheet);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getConfig(String key) {
        return configuration.get(key);
    }

    public PartConfiguration getPartConfiguration() {
        return partConfiguration;
    }

    public ReqStatusPartConfiguration getReqStatusPartConfiguration() {
        return reqStatusPartConfiguration;
    }
}
