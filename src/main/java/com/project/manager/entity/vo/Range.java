package com.project.manager.entity.vo;

import lombok.Builder;
import lombok.Data;

/**
 * 范围
 *
 * @author zhaohui
 */
@Data
@Builder
public class Range<T> {

    private T begin;

    private T end;

}
