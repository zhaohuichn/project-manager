package com.project.manager.entity.po;


import com.project.manager.entity.vo.Range;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 排期条目
 *
 * @author zhaohui
 */
@Data
@Builder
public class RequirementScheduleItem {
    /**
     * 是否已经结束
     */
    private boolean isDone;
    /**
     * 日期范围
     */
    private Range<String> dateRange;
    /**
     * 部门
     */
    private List<String> part;

}