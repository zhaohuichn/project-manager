package com.project.manager.entity.po;

/**
 * 需求人员
 *
 * @author ZHAOHUI
 */
public class RequirementMember {
    /**
     * 真实业务组
     */
    private int realBizGroup;
    /**
     * 临时项目组
     */
    private String tempProjectGroup;
    /**
     * 成员名称
     */
    private String memberName;
    /**
     * 负责级别
     */
    private String principleLevel;

}
