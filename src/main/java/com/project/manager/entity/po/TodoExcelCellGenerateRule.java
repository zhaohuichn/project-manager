package com.project.manager.entity.po;

import com.project.manager.source.excel.row.ExcelRow;
import lombok.Builder;
import lombok.Data;

/**
 * 待办资源事项生成规则
 *
 * @author zhaohui
 */
@Data
@Builder
public class TodoExcelCellGenerateRule {

    private ExcelRow srcExcelRow;

    private String targetCellName;

    private String srcCellName;


}
