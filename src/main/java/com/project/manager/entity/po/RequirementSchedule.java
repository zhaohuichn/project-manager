package com.project.manager.entity.po;

import com.google.common.collect.Range;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 排期
 *
 * @author zhaohui
 */
@Data
@Builder
public class RequirementSchedule {

    private Range<String> dateRange;

    private List<RequirementScheduleItem> items;


}