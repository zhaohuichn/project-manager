package com.project.manager.entity.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 输出数据
 *
 * @author ZHAOHUI
 */
public class OutputExcelSheetDataRow implements Serializable {

    private List<OutputExcelSheetDataCell> cells;

}
