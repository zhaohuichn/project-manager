package com.project.manager.entity.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 输出数据
 *
 * @author ZHAOHUI
 */
public class OutputExcelSheetData implements Serializable {

    private List<OutputExcelSheetDataRow> rowDataList;

}
