package com.project.manager;

import com.project.manager.builder.TodoExcelBuilder;
import com.project.manager.parse.excel.StandardExcelParser;
import com.project.manager.source.excel.Excel;
import com.project.manager.util.ExcelUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;

/**
 * ExcelController
 *
 * @author ZHAOHUI
 */
@RestController
public class ExcelController {

    @Resource
    private ConfigService configService;

    /**
     * @return
     */
    @GetMapping("/todo")
    public String todo() {
        File file = new File(configService.getConfig("inputFilePath"));
        Excel inputFile = new StandardExcelParser().parse(file);

        String outFile = configService.getConfig("outputFilePath");
        TodoExcelBuilder builder = new TodoExcelBuilder(inputFile);
        Excel todoExcel = builder.build();

        ExcelUtil.writeWorkbook(todoExcel, outFile);
        return "Done";
    }

}
