package com.project.manager.enums;

/**
 * ConfigSheetEnum
 *
 * @author ZHAOHUI
 */
public enum ConfigSheetEnum {

    TODO_TYPE(1, "需求状态&代办类型映射"),
    LOCATION_SOURCE_FILE(2, "sysConfig"),
    CONFIG_SCHEDULE_CONTENT_PART(3, "partConfig"),
    CONFIG_SCHEDULE_STATUS_PART(4, "reqStatusPartConfig")
    ;

    private int index;

    private String name;

    ConfigSheetEnum(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
