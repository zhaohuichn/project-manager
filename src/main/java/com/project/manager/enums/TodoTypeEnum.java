package com.project.manager.enums;

/**
 * 待办类型
 *
 * @author zhaohui
 */
public enum TodoTypeEnum {

    NOTICE, CONFIRM;

}
