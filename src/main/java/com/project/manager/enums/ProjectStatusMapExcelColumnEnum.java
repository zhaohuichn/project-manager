package com.project.manager.enums;

/**
 * TodoRuleExcelColumnEnum
 *
 * @author ZHAOHUI
 */
public enum ProjectStatusMapExcelColumnEnum {
    REQUIREMENT_STATUS(0, "需求状态"),
    STANDARD_STATUS(1, "标准状态"),
    TODO_TYPE(2, "待办类型"),
    TODO_CONTENT_TEMPLATE(3, "待办内容模板")
    ;

    private int index;

    private String name;

    ProjectStatusMapExcelColumnEnum(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
