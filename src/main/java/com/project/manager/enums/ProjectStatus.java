package com.project.manager.enums;

/**
 * 项目状态
 * <p>
 * 启动 -> 需求 -> 开发 -> 测试 -> 验收 -> 上线  -> 结束
 * <p>
 * 项目启动 -> 需求评审
 * -> 待技术评审 -> 技术评审 -> 待确认排期 -> 待开发 -> 正在开发 -> 正在联调 -> 提测
 * -> 待测试 -> 正在测试 -> 测试完成
 * -> 待验收 -> 验收完成 -> 待上线 -> 已上线 -> 项目结束
 *
 * @author ZHAOHUI
 */
public enum ProjectStatus {
    /**
     * 项目相关
     */
    PROJECT_START(100, "项目启动"),
    PROJECT_FINISH(101, "项目结束"),
    PROJECT_PROPEL(102, "正在推进"),
    PROJECT_PAUSE(103, "项目暂停"),
    PROJECT_SUSPEND(104, "项目挂起"),
    PROJECT_REASSESS(105, "待重新评审"),
    PROJECT_SCHEDULE(106, "待确认排期"),
    /**
     * 需求相关
     */
    REQ_DONE(200, "已需求评审"),
    REQ_NEED_DETERMINE(201, "待确认需求"),
    /**
     * 开发相关
     */
    DEV_DOING(300, "正在开发"),
    DEV_PLAN_DISCUSS(301, "待确认技术方案"),
    DEV_PLAN_ASSESS(302, "待技术评审"),
    DEV_START_PRE(303, "待开发"),
    DEV_TESTING_SELF(304, "正在自测"),
    DEV_DEBUGGING_JOINT(305, "正在联调"),
    DEV_FINISH(306, "开发完成"),
    DEV_DATA_PUSHING(307, "正在推数"),
    /**
     * 测试相关
     */
    TEST_DOING(400, "正在测试"),
    TEST_CASE_DISCUSS(401, "测试CASE梳理"),
    TEST_CASE_ASSESS(402, "待测试CASE评审"),
    TEST_START_PRE(403, "待测试"),
    TEST_FINISH(404, "测试完成"),
    /**
     * 验收相关
     */
    ACCEPT(500, "验收相关"),
    ACCEPTING_OFFLINE_DOING(501, "正在验收"),
    /**
     * 上线相关
     */
    ONLINE(600, "已上线"),
    ONLINE_PRE(601, "待上线"),
    ONLINE_DOING(601, "上线灰度中"),
    ONLINE_WAIT_NOTICE(602, "待产品通知上线"),
    ONLINE_ACCEPT_PRE(603, "待线上试单"),
    ONLINE_ACCEPTING(604, "正在线上试单"),
    ;

    private int code;

    private String name;

    ProjectStatus(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
