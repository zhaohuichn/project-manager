package com.project.manager.enums;

/**
 * TodoSheetColumnEnum
 *
 * @author ZHAOHUI
 */
public enum TodoSheetColumnEnum {


    REQ_STATUS(1, "需求状态"),
    STANDARD_STATUS(2, "标准状态"),
    TODO_TYPE(3, "待办类型"),
    TODO_TEXT(4, "文案模板");

    private int index;

    private String name;

    TodoSheetColumnEnum(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
